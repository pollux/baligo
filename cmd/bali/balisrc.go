package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
)

// BaliSrcMetadata todo
type BaliSrcMetadata struct {
	Name        string   `json:"name"`
	Destination string   `json:"destination,omitempty"`
	Version     string   `json:"version,omitempty"`
	GoFlags     []string `json:"goflags,omitempty"`
}

// BaliFile todo
type BaliFile struct {
	Path        string `json:"path"`
	Destination string `json:"destination"`
}

// Install todo
func (file *BaliFile) Install(workdir, outdir string) error {
	fileoutdir := filepath.Join(outdir, file.Destination)
	name := filepath.Base(file.Path)
	_ = os.MkdirAll(fileoutdir, 0775)
	outfile := filepath.Join(fileoutdir, name)
	srcfile := filepath.Join(workdir, file.Path)
	if PathExists(outfile) && !IsForceMode {
		if !IsForceMode {
			return nil
		}
		fmt.Fprintf(os.Stderr, "update \x1b[32m%s\x1b[0m\n", outfile)
	} else {
		fmt.Fprintf(os.Stderr, "install \x1b[32m%s\x1b[0m\n", outfile)
	}
	return CopyFile(srcfile, outfile)
}

// BaliMetadata  todo
type BaliMetadata struct {
	Name         string     `json:"name"`
	Version      string     `json:"version,omitempty"`
	Files        []BaliFile `json:"files,omitempty"`
	Dirs         []string   `json:"dirs,omitempty"`
	DirFallback  []string   `json:"Dirs,omitempty"`
	FileFallback []BaliFile `json:"install,omitempty"`
}

// FileInstall todo
func (bm *BaliMetadata) FileInstall(workdir, outdir string) error {
	for _, file := range bm.Files {
		if err := file.Install(workdir, outdir); err != nil {
			return err
		}
	}
	return nil
}

// LoadMetadata todo
func LoadMetadata(file string, v interface{}) error {
	fd, err := os.Open(file)
	if err != nil {
		return err
	}
	defer fd.Close()
	if err := json.NewDecoder(fd).Decode(v); err != nil {
		return err
	}
	return nil
}
