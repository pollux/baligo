# Baligo - Bali re-implemented with golang


When developing a project based on Golang, although `go build` can be used to easily complete the build, I still encountered some troubles during packaging and other operations. Therefore, I implemented a cross-platform tool based on PowerShell Core [bali](https://gitee.com/ipvb/bali/tree/pwsh), to simplify this process, as early as 2017, bali was born, and now has reached 2020, I also have a new understanding of project construction and packaging , So in order to improve bali written based on PowerShell, bali was rewritten using Golang in this project.


## Usage

baligo usage：

```txt
Bali - Minimalist build and packaging tool
usage: bali <option> args ...
  -h|--help        Show usage text and quit
  -v|--version     Show version number and quit
  -V|--verbose     Make the operation more talkative
  -F|--force       Turn on force mode. eg: Overwrite configuration file
  -c|--cwd         Build dir. (Position 0, default cwd)
  -a|--arch        Build arch: amd64 386 arm arm64
  -t|--target      Build target: windows linux darwin ...
  -o|--out         Build output dir. default '$CWD/build'
  -z|--zip         Create archive file after successful build
  -m|--mkstgz      After successful build, create STGZ installation package
  -d|--dist        STGZ/TarGz package distribution directory

```


Build:

```shell
bali /path/to/project
```

Create `Tar.gz`:

```shell
bali /path/to/project -z
```

Create `STGZ` installation package, mainly used on Linux/macOS platform:

```shell
bali /path/to/project -m
```

## Bali Project files

There are two types of Bali project files, including `bali.json` in the root directory of the project and` balisrc.json` file in the specific program of the project. Examples are as follows:

`bali.json`:

```js
{
  // Package name
    "name": "baligo",
    // Package version
    "version": "1.0.0",
    // Project profiles
    "files": [
        {
            "path": "config/bali.json",
            "destination": "config"
        }
    ],
    // Sources dir
    "dirs": [
        "cmd/bali"
    ]
}
```

`balisrc.json`:

```js
{
    // Binary name
    "name": "bali",
    // destination
    "destination": "bin",
    // binary version
    "version": "1.0.0",
    // Goflags, each one will be expanded using environment variables, BUILD_VERSION corresponds to version, BUILD_TIME is the RFC3339 format of local time
    // BUILD_GOVERSION is go version to remove the prefix
    // git commit information of BUILD_COMMIT project, use None instead for non-go repository.
    "goflags": [
        "-ldflags",
        "-X 'main.VERSION=$BUILD_VERSION' -X 'main.BUILDTIME=$BUILD_TIME' -X 'main.BUILDCOMMIT=$BUILD_COMMIT' -X 'main.GOVERSION=$BUILD_GOVERSION'"
    ]
}
```